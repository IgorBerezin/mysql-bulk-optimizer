/**
 * Removing last semicolon from sql query
 * @param {String} str sql query string
 * @returns {String} clean string
 */
module.exports.cleanLastSemicolon = (str) => {
	return str.replace(/\s*;\s*/g, '');
};

module.exports.emptyFunction = function() {};