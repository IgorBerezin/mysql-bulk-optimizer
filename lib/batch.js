const uuidv1 = require('uuid/v1');
const Operation = require('./operation');
const bulk = require('./bulk');
const utils = require('./utils');


const DEFAULT_BATCH_SIZE = 100;
const MAX_BATCH_SIZE = 1000;
const DEFAULT_WAIT_TIME = 30000; // wait time in milliseconds (30 seconds)

/*
Support for multiple statements is disabled for security reasons (it allows for SQL injection attacks if values are not properly escaped). To use this feature you have to enable it for your connection:

var connection = mysql.createConnection({multipleStatements: true});

The simplest form of .query() is .query(sqlString, callback), where a SQL string is the first argument and the second is a callback:

The second form .query(sqlString, values, callback) comes when using placeholder values (see escaping query values):


*/


/**
 * Main Batch package
 * Represents class to batch MySQL queries using bulk operations.
 * @return {Batch}
 */
const Batch = module.exports = class Batch {
	/**
	 * Construct a new Batch over db object
	 *
	 * @constructor
	 * @param {Object} db Mysql connection or pool
	 * @param {Object} options Batch options
	 * @param {Number} options.size batch size
	 * @param {Number} options.time maxWaitTime
	 * @param {String} options.comment comment for a batch reason
	 */
	constructor(db, {
		size: batchSize = DEFAULT_BATCH_SIZE,
		time: maxWaitTime = DEFAULT_WAIT_TIME,
		comment
	} = {}) {
		if (typeof db.query !== 'function') {
			throw new TypeError('Batch works only over Pool or Connection object');
		}

		if (db.state === 'disconnected') {
			throw new TypeError('Batch works only over active connection');
		}


		this.BatchSize = batchSize;
		this.MaxWaitTime = maxWaitTime;
		this.Comment = comment;
		this._id = uuidv1();

		//it is necessary to execute all queries before ending.
		if (typeof db.end === 'function') {
			if (db._batches === undefined) {
				db._batches = [];
			}
			db._batches.push(this);

			if (db._endOriginal === undefined) {
				db._endOriginal = db.end;
				db.end = function () {
					const self = this;
					const args = arguments;

					const nextExecute = (idx, cb) => {
						if (idx >= db._batches.length || idx > 1000) {
							return cb();
						}

						const btch = db._batches[idx];
						idx++;
						btch._closed = true;
						if (btch.Quantity) {
							btch.execute(function () {
								nextExecute(idx, cb);
							});
						} else {
							nextExecute(idx, cb);
						}
					};

					return nextExecute(0, () => {
						db.end = db._endOriginal;
						return db.end.apply(self, args);
					});
				};
			}
		}

		this._db = db;
		this._items = [];
		this._waitForTimeout = false;
		this._executing = false;
	}

	/**
	 * Is batch closed
	 */
	get Closed() {
		return this._closed;
	}

	/**
	 * Quantity of queries inside this batch
	 */
	get Quantity() {
		return this.Items.length;
	}

	/**
	 * Array of bulk operations
	 */
	get Items() {
		return this._items;
	}

	/**
	 * Get: Size of batch operation
	 */
	get BatchSize() {
		return this._batchSize;
	}

	/**
	 * Set: Size of batch operation
	 * @param {Number} value
	 */
	set BatchSize(value) {
		if (isNaN(value)) {
			throw new Error('Wrong size value');
		}
		let size = Number(value);
		if (size > MAX_BATCH_SIZE) {
			throw new Error('Maximum batch size exceeded');
		}
		return this._batchSize = size;
	}

	/**
	 * Get: Maximum time wait before executing of batch operation
	 */
	get MaxWaitTime() {
		return this._maxWaitTime;
	}

	/**
	 * Set: Maximum time wait before executing of batch operation
	 * @param {Number} value
	 */
	set MaxWaitTime(value) {
		if (isNaN(value)) {
			throw new Error('Wrong time value');
		}
		let time = Number(value);

		return this._maxWaitTime = time;
	}

	/**
	* Get: Comment for a batch reason
	*/
	get Comment() {
		return this._comment;
	}

	/**
	 * Set: Comment for a batch reason
	 * @param {String} value
	 */
	set Comment(value) {
		return this._comment = value;
	}
	/**
	 * Override mysql end with bulk operation
	 * @param {Function} cb
	 */
	end(cb) {
		cb = cb || utils.emptyFunction; // do nothing if not defined1.
		this._db.end.apply(this, [cb]);
	}

	/**
	 * Override mysql query with bulk operation
	 * @param {String} sql
	 * @param {Array || Function} params
	 * @param {Function} cb
	 */
	query(sql, params, cb) {
		if (typeof params === 'function') {
			cb = params;
			params = undefined;
		}
		cb = cb || utils.emptyFunction; // do nothing if not defined2.
		if (this.Closed) return cb();


		if (!sql) {
			return cb();
		}

		this.collect(new Operation(sql, params, cb));

		if (this.Quantity >= this.BatchSize) {
			this.execute();
		}

		return this;
	}

	/**
	 * Add operation to the current batch
	 * @param {Operation} operation operation
	 */
	addToBatch(operation) {
		if (!(operation instanceof Operation)) {
			throw new TypeError('Batch items: Unexpected operation to add');
		}

		if (this.Closed) return;

		this._items.push(operation);
		if (!this._waitForTimeout) {
			this.startWaiting();
		}
	}

	/**
	 * Collect new operation
	 * @param {Operation} candidate
	 */
	collect(candidate) {
		for (let i = 0; i < this._items.length; i++) {
			const operation = this._items[i];

			let bulkOperation = bulk(operation, candidate);
			if (bulkOperation) {
				this._items[i] = bulkOperation;
				return;
			}

		}
		this.addToBatch(candidate);
	}

	/**
	 * Real execution of batch.
	 * @returns {Number} amount of executed queries
	 */
	execute(cb) {
		if (this._executing) {
			if (cb) {
				return cb(undefined, 0);
			}
			return 0;
		}

		this.stopWaiting();

		this._executing = true;

		let amount = this.Quantity;
		let items = this.Items.splice(0, amount);

		let sql = '', params = [];
		for (let i = 0; i < amount; i++) {
			const operation = items[i];
			sql += utils.cleanLastSemicolon(operation.sql) + ';';
			params = params.concat(operation.params || []);
		}
		let self = this;

		this._db.query(sql, params, function (error, results, fields) {
			let that = this;
			for (let i = 0; i < amount; i++) {
				const operation = items[i];
				const result = (results && results.length > i ? results[i] : undefined);
				if (typeof operation.callback === 'function') {
					operation.callback.apply(that, [error, result, fields]);
				}
			}
			self._executing = false;
			if (cb) {
				return cb(undefined, amount);
			}
		});
		return amount;
	}

	startWaiting() {
		this._waitForTimeout = true;
		if (this.MaxWaitTime > 0) {
			this._timeOut = setTimeout(() => {
				this.execute();
			}, this.MaxWaitTime);
		}
	}

	stopWaiting() {
		if (this._timeOut) {
			clearTimeout(this._timeOut);
		}

		this._waitForTimeout = false;
	}
};
