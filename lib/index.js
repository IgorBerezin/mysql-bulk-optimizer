const Batch = require('./batch');
const Operation = require('./operation');

module.exports = {
	Batch: Batch,
	Operation: Operation,
	createBatch: function (db, options) {
		return new Batch(db, options);
	}
};