/**
 * Operation container for a query.
 * @return {Operation}
 */
const Operation = module.exports = class Operation {

	/**
	 * Construct a new Operation container for query
	 * @summary
	 * It is a container for params of original .query function
	 *
	 * The simplest form of .query() is
	 * .query(sqlString, callback),
	 * where a SQL string is the first argument and the second is a callback
	 *
	 * The second form
	 * .query(sqlString, values, callback)
	 * comes when using placeholder values.
	 *
	 * @constructor
	 * @param {String} sql
	 * @param {Array || Function} params
	 * @param {Function} cb
	 */
	constructor(sql, params, cb) {
		if (typeof params == 'function') {
			cb = params;
			params = undefined;
		}

		this._sql = sql;
		this._params = params;
		this._cb = cb;
		this._IsInsert = false;
		this._IsDelete = false;
		this._key = undefined;
		this.parse();
	}

	get sql() {
		return this._sql;
	}

	get params() {
		return this._params;
	}

	get callback() {
		return this._cb;
	}

	get key() {
		return this._key;
	}

	get IsInsert() {
		return this._IsInsert;
	}

	get IsDelete() {
		return this._IsDelete;
	}

	get commonSQL() {
		return this._commonSQL;
	}

	get valuesSQL() {
		return this._valuesSQL;
	}

	parse() {
		let key = this._sql.toUpperCase();

		if (key.startsWith('INSERT INTO')) {
			this._IsInsert = true;

			let parts = this._sql.split(/\)\s*(VALUES)\s*\(/i);
			if (parts.length === 3) {
				//INSERT INTO table_name (c1,c2,c3) VALUES
				this._commonSQL = parts[0] + ') VALUES ';
				//Should I check for '?' placeholder inside 'insert into' query?

				//(?,?,?)
				this._valuesSQL = '(' + parts[2];
			}
		} else if (key.startsWith('DELETE FROM')) {
			this._IsDelete = true;
			//hrow new Error('Not implemented Delete query');
		}

		this._key = this._commonSQL ? this._commonSQL.toUpperCase() : key;
	}

};