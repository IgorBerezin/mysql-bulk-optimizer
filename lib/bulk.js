const Operation = require('./operation');
const utils = require('./utils');

/**
 * Method to create a bulk operation
 * @param {Operation} operation
 * @param {Operation} candidate
 * @returns {Operation} bulk operation
 */
module.exports = function (operation, candidate) {
	if (!operation) {
		return null;
	}

	if (!(operation instanceof Operation)) {
		throw new TypeError('Wrong argument for join');
	}

	if (!candidate) {
		return operation;
	}

	if (operation.key !== candidate.key) {
		return null;
	}

	if (operation.commonSQL) {
		const batchSQL = operation.commonSQL + utils.cleanLastSemicolon(operation.valuesSQL) + ', ' + candidate.valuesSQL;
		const batchParams = (operation.params || []).concat(candidate.params || []);
		const callback = function () {
			if (operation.callback !== null) {
				operation.callback.apply(this, arguments);
			}

			if (candidate.callback !== null) {
				candidate.callback.apply(this, arguments);
			}
		};

		return new Operation(batchSQL, batchParams, callback);
	// } else if (this.IsDelete) {
	// 	throw new Error('Not implemented Delete join');
	// } else {
	// 	throw new Error('Join operation is available only for INSER/DELETE queries');
	}
	return null;
};
