const Operation = require('../lib/operation');
const bulk = require('../lib/bulk');
const helper = require('./helper');

describe('bulk tests', function () {
	describe('#', function () {
		it('should return null for empty operation', function () {
			let operation = bulk(null, new Operation('select', []));
			(operation === null).should.be.true();
		});
		it('should throw exception for wrong operation', function () {
			let error = new Error('Fake');
			try {
				bulk({}, new Operation('select', []));
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong argument for join');
		});
		it('should return operation for empty candidate', function () {
			let input = new Operation('select', []);
			let output = bulk(input);
			output.should.be.eql(input);
		});
		it('should return null for operations with different sql', function () {
			let input = new Operation('select', []);
			let candidate = new Operation('update', []);
			let output = bulk(input, candidate);
			(output === null).should.be.true();
		});
		it('should return null for same operations without bulk', function () {
			let input = new Operation('select', []);
			let candidate = new Operation('select', []);
			let output = bulk(input, candidate);
			(output === null).should.be.true();
		});
		it('should make a bulk query', async function () {
			let sql = 'INSERT INTO table_name (c1,c2,c3) VALUES (?, ?, ?);';
			let bulkSql = 'INSERT INTO table_name (c1,c2,c3) VALUES (?, ?, ?), (?, ?, ?);';
			let params = [1, 2, 3];
			let bulkParams = [1, 2, 3, 1, 2, 3];

			let input = new Operation(sql, params);
			let candidate = new Operation(sql, params);
			let output = bulk(input, candidate);
			output.sql.should.equal(bulkSql);
			output.params.should.eql(bulkParams);
		});
		it('should invoke all callbacks', async function () {
			let sql = 'INSERT INTO table_name (c1,c2,c3) VALUES (?, ?, ?);';
			let spy = helper.sinon.spy();
			let params = [1, 2, 3];

			let input = new Operation(sql, params, spy);
			let candidate = new Operation(sql, params, spy);
			let output = bulk(input, candidate);
			output.callback();
			spy.callCount.should.equal(2);
		});
		it('should skip empty callback', async function () {
			let sql = 'INSERT INTO table_name (c1,c2,c3) VALUES (?, ?, ?);';
			let params = [1, 2, 3];

			let input = new Operation(sql, params, () => {});
			let spyInput = helper.sinon.spy(input, '_cb');
			let candidate = new Operation(sql, params, () => { });
			let spyCandidate = helper.sinon.spy(candidate, '_cb');
			let output = bulk(input, candidate);
			input._cb = null;
			candidate._cb = null;
			output.callback();
			spyInput.callCount.should.equal(0);
			spyCandidate.callCount.should.equal(0);
		});
		it('should works with empty params', async function () {
			let sql = 'INSERT INTO table_name (c1,c2,c3) VALUES (?, ?, ?);';

			let input = new Operation(sql);
			let candidate = new Operation(sql);
			let output = bulk(input, candidate);
			output.params.should.eql([]);
		});
	});
});