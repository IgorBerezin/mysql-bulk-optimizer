const Operation = require('../lib/operation');
const helper = require('./helper');

describe('operation tests', function () {
	describe('#constructor', function () {
		it('should create Operation instance', function () {
			let operation = new Operation('select', []);
			operation.should.be.instanceof(Operation);
		});
		it('should use callback as second argument if it is a function', async function () {
			let sql = 'SELECT';
			let f = () => { };
			let operation = new Operation(sql, f);

			operation.callback.should.equal(f);
		});
		it('should call parse method', async function () {
			let sql = 'INSERT INTO table_name (c1,c2,c3) VALUES (?, ?, ?);';
			let params = [1, 2, 3];

			let spy = helper.sinon.spy(Operation.prototype, 'parse');
			let operation = new Operation(sql, params);
			operation.parse();
			spy.callCount.should.equal(2);
		});
	});
	describe('#parse', function () {
		it('should fill key value', function () {
			let operation = new Operation('select', []);
			operation.key.should.be.equal('SELECT');
		});
		it('should fill isInsert value', function () {
			let sql = 'INSERT INTO table_name (c1,c2,c3) VALUES (?, ?, ?);';
			let operation = new Operation(sql, []);
			operation.IsInsert.should.be.true();
		});
		it('should fill isDelete value', function () {
			let sql = 'DELETE FROM `table_name` WHERE `primary_key` in (?)';
			let operation = new Operation(sql, []);
			operation.IsDelete.should.be.true();
		});
		it('should ignore Inser query without values', function () {
			let sql = 'INSERT INTO table_name (c1,c2,c3);';
			let operation = new Operation(sql, []);
			(!operation.commonSQL).should.be.true();
		});
	});
});