const utils = require('../lib/utils');
const { promisify } = require('util');
const lib = require('../lib');
const helper = require('./helper');
const sinon = require('sinon');

let connection;

describe('batch tests', function () {
	before(function () {
		connection = helper.getFakeMysqlConnection();
	});
	after(function () {
		helper.mockMysql.restore();
	});
	describe('#constructor', function () {
		it('should create Batch instance', function () {
			let batch = lib.createBatch(connection, {});
			batch.should.be.instanceof(lib.Batch);
		});
		it('should change Batch size', function () {
			let size = 10;
			let batch = lib.createBatch(connection, { size: size });
			batch.BatchSize.should.equal(size);
		});
		it('should change Max wait time', function () {
			let time = 10;
			let batch = lib.createBatch(connection, { time: time });
			batch.MaxWaitTime.should.equal(time);
		});
		it('should change comment', function () {
			const batch = lib.createBatch(connection, { comment: 'test comment' });
			batch.Comment.should.equal('test comment');
		});

		it('should throw exception for wrong connection', function () {
			let error = new Error('Fake');
			try {
				lib.createBatch({});
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Batch works only over Pool or Connection object');
		});
		it('should throw exception for not active connection', function () {
			let error = new Error('Fake');
			try {
				let fakeconnection = helper.getFakeMysqlConnection();
				fakeconnection.state = 'disconnected';
				lib.createBatch(fakeconnection, {});
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Batch works only over active connection');
		});
		it('should throw exception for wrong Batch size', function () {
			let error = new Error('Fake');
			try {
				lib.createBatch(connection, { size: 'size' });
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong size value');
		});
		it('should throw exception for very big Batch size', function () {
			let error = new Error('Fake');
			try {
				lib.createBatch(connection, { size: 1001 });
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Maximum batch size exceeded');
		});
		it('should throw exception for wrong Max wait time', function () {
			let error = new Error('Fake');
			try {
				lib.createBatch(connection, { time: 'time' });
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong time value');
		});
		describe('connection upgrades', () => {
			let emptyConnection;

			beforeEach(() => {
				emptyConnection = {
					query: function() {},
					end: function (cb) {}
				};
			});

			it('connection.batches should be empty Array if connection.end === undefined',  () => {
				const connectionWithoutEnd = {
					query: function () {},
				};
				lib.createBatch(connectionWithoutEnd);
				connectionWithoutEnd.should.not.have.property('_batches');
			});
			it('should set connection.batches to Array if property not exist',  () => {
				emptyConnection.should.not.have.property('_batches');
				lib.createBatch(emptyConnection);
				emptyConnection._batches.should.be.instanceof(Array);
			});
			it('created batch should has been added to connection._batches',  () => {
				lib.createBatch(emptyConnection);
				emptyConnection._batches.length.should.be.equal(1);
			});
			it('if db._endOriginal not exists - should equal db.end', () => {
				const oldEndFn = emptyConnection.end;
				const batch = lib.createBatch(emptyConnection);
				batch._db._endOriginal.toString().should.be.equal(oldEndFn.toString());
			});
			it('if db._endOriginal not exists - db.end should be another function', () => {
				const oldEndFn = emptyConnection.end;
				const batch = lib.createBatch(emptyConnection);
				batch._db.end.toString().should.be.not.equal(oldEndFn.toString());
			});
			it('should doesnt call batch.execute if batch doesnt have Quantity when db._endOriginal is not a function', () => {
				const batch = lib.createBatch(emptyConnection);
				const executeFnSpy = sinon.spy(batch, 'execute');

				batch.end(() => {});
				executeFnSpy.calledOnce.should.be.false();
			});
			it('should call batch.execute if batch have Quantity when db._endOriginal is not a function', () => {
				const batch = lib.createBatch(emptyConnection);
				const executeFnSpy = sinon.spy(batch, 'execute');

				let sql = 'SELECT';
				let operation = new lib.Operation(sql, []);
				batch.addToBatch(operation);

				batch.end(() => {});
				executeFnSpy.calledOnce.should.be.true();
			});
		});
	});
	describe('#execute', function () {
		it('should ignore executed batch', async function () {
			let batch = lib.createBatch(connection, { comment: 'should ignore executed batch'});
			let sql = 'SELECT';
			let operation = new lib.Operation(sql, []);
			batch.addToBatch(operation);
			batch._executing = true;
			let amount = batch.execute();
			amount.should.equal(0);
			batch.stopWaiting();
			batch._executing = false;
		});
	});
	describe('#startWaiting', function () {
		it('should be invoked if new operation added', async function () {
			let sql = 'SELECT';
			let batch = lib.createBatch(connection, { comment: 'should be invoked if new operation added' });

			let spy = helper.sinon.spy(batch, 'startWaiting');
			let operation = new lib.Operation(sql, []);
			batch.addToBatch(operation);
			batch.stopWaiting();
			spy.callCount.should.equal(1);
		});
		it('should execute batch once the wait is over', async function () {
			let sql = 'SELECT';
			let time = 100;
			let batch = lib.createBatch(connection, { time: time, comment: 'should execute batch once the wait is over'});

			let spy = helper.sinon.spy(batch, 'execute');
			let operation = new lib.Operation(sql, []);
			batch.addToBatch(operation);

			const setTimeoutPromise = timeout => {
				return new Promise(resolve => setTimeout(resolve, timeout));
			};

			await setTimeoutPromise(2 * time);
			spy.callCount.should.equal(1);
		});
		it('should never execute batch if waitTime is zero', async function () {
			let sql = 'SELECT';
			let time = 0;
			let batch = lib.createBatch(connection, { time: time, comment: 'should never execute batch if waitTime is zero' });

			let spy = helper.sinon.spy(batch, 'execute');
			let operation = new lib.Operation(sql, []);
			batch.addToBatch(operation);

			const setTimeoutPromise = timeout => {
				return new Promise(resolve => setTimeout(resolve, timeout));
			};

			await setTimeoutPromise(2 * time);
			spy.callCount.should.equal(0);
		});
	});
	describe('#stopWaiting', function () {
		it('should skip clearTimeout if timeout is not present', async function () {
			let sql = 'SELECT';
			let time = 0;
			let batch = lib.createBatch(connection, { time: time, comment: 'should skip clearTimeout if timeout is not present' });
			let clock = helper.sinon.useFakeTimers();
			let spy = helper.sinon.spy(clock, 'clearTimeout');
			let operation = new lib.Operation(sql, []);
			batch.addToBatch(operation);
			batch.stopWaiting();
			spy.callCount.should.equal(0);
			spy.restore();
		});
	});
	describe('#end', function () {
		it('if end calls without cb - cb should be empty function', () => {
			const connectionForEnd = helper.getFakeMysqlConnection();
			const db = helper.spyMysqlConnection(connectionForEnd);

			const batch = lib.createBatch(connectionForEnd);
			batch.end();

			db.end.calledOnce.should.be.true();
			db.end.args[0].toString().should.be.equal(utils.emptyFunction.toString());
		});
		it('should execute all queries before ending', async function () {
			let queries = [
				'SELECT 1 as id',
				'SELECT 2 as id'
			];
			let results = [{ id: 1 }, { id: 2 }];
			let fields = ['id'];
			const connectionForEnd = helper.getFakeMysqlConnection();

			let db = helper.mockMysqlConnection(connectionForEnd);

			db.expects('query')
				.withArgs(queries.join(';') + ';', [])
				//.once()
				.callsArgWith(2, null, results, fields);

			let batch = lib.createBatch(connectionForEnd, { size: 3, comment: 'should execute all queries before ending' });


			const asyncQuery = (sql, params) => {
				return new Promise(function (resolve, reject) {
					batch.query(sql, params, function (err, result) {
						if (err) {
							return reject(err);
						}
						resolve(result);
					});
				});
			};

			let res = {};
			const f = async (idx) => {
				res[idx] = await asyncQuery(queries[idx], []);
			};

			const promiseEnd = promisify(batch.end).bind(batch);

			const fend = async () => {
				await promiseEnd();
			};

			await Promise.all([f(0), f(1), fend()]);
			(res['0'].id === 1).should.be.true();
			(res['1'].id === 2).should.be.true();
			batch.Items.length.should.equal(0);
			db.restore();
		});
		it('should execute all queries from all batches before ending', async function () {
			let queries = [
				'SELECT 1 as id;'
			];
			let results = [{ id: 1 }];
			let fields = ['id'];
			const connectionForEnd = helper.getFakeMysqlConnection();

			let db = helper.mockMysqlConnection(connectionForEnd);
			db.expects('query')
				.withArgs(queries[0], [])
				.twice()
				.callsArgWith(2, null, results, fields);

			let batchFirst = lib.createBatch(connectionForEnd, { size: 2, comment: 'should execute all queries from all batches before ending (first)' });
			let batchSecond = lib.createBatch(connectionForEnd, { size: 2, comment: 'should execute all queries from all batches before ending (second)' });
			//console.log(connectionForEnd);


			const asyncQuery = (sql, params, batch) => {
				return new Promise(function (resolve, reject) {
					batch.query(sql, params, function (err, result) {
						if (err) {
							return reject(err);
						}
						resolve(result);
					});
				});
			};

			let res = {};
			const f = async (idx) => {
				const batch = idx === 0 ? batchFirst : batchSecond;
				res[idx] = await asyncQuery(queries[0], [], batch);
			};

			const promiseEnd = promisify(connectionForEnd.end).bind(connectionForEnd);

			const fend = async () => {
				await promiseEnd();
			};

			await Promise.all([f(0), f(1), fend()]);
			(res['0'].id === 1).should.be.true();
			(res['1'].id === 1).should.be.true();
			batchFirst.Items.length.should.equal(0);
			batchSecond.Items.length.should.equal(0);
			db.restore();
		});
	});
	describe('#query', function () {
		it('should add query to list', async function () {
			let sql = 'SELECT';
			let batch = lib.createBatch(connection, { comment: 'should add query to list'});
			batch.query(sql, []);

			batch.Quantity.should.equal(1);
			batch.Items.length.should.equal(1);
			batch.Items[0].sql.should.equal(sql);
			batch.execute();

		});
		it('should use callback as second argument if it is a function', async function () {
			let sql = 'SELECT';
			let f = () => {};
			let batch = lib.createBatch(connection, { comment: 'should use callback as second argument if it is a function'});
			batch.query(sql, f);

			batch.Items[0].callback.should.equal(f);
			batch.execute();

		});
		it('should skip empty query', async function () {
			let sql = '';
			let batch = lib.createBatch(connection, {});
			batch.query(sql, []);

			batch.Quantity.should.equal(0);
			batch.Items.length.should.equal(0);
		});
		it('should execute when limit is reached', async function () {
			let queries = [
				'SELECT 1 as id',
				'SELECT 2 as id'
			];
			let results = [{ id: 1 }, { id: 2 }];
			let fields = ['id'];
			let db = helper.mockMysqlConnection(connection);

			db.expects('query')
				.withArgs(queries.join(';') + ';', [])
				//.once()
				.callsArgWith(2, null, results, fields);

			let batch = lib.createBatch(connection, { size: 2 });

			const asyncQuery = (sql, params) => {
				return new Promise(function (resolve, reject) {
					batch.query(sql, params, function (err, result) {
						if (err) {
							return reject(err);
						}
						resolve(result);
					});
				});
			};
			asyncQuery(queries[0], []).should.be.finally.eql(results[0]);
			asyncQuery(queries[1], []).should.be.finally.eql(results[1]);
			batch.Quantity.should.equal(0);
			batch.Items.length.should.equal(0);
			db.restore();
		});
		it('should return empty result for each callback if bulk result is empty', async function () {
			let queries = [
				'SELECT 1 as id',
				'SELECT 2 as id'
			];
			let results = null;
			let fields = ['id'];
			let db = helper.mockMysqlConnection(connection);

			db.expects('query')
				.withArgs(queries.join(';') + ';', [])
				//.once()
				.callsArgWith(2, null, results, fields);

			let batch = lib.createBatch(connection, { size: 2 });


			const asyncQuery = (sql, params) => {
				return new Promise(function (resolve, reject) {
					batch.query(sql, params, function (err, result) {
						if (err) {
							return reject(err);
						}
						resolve(result);
					});
				});
			};

			let res = {};
			const f = async (idx) => {
				res[idx] = await asyncQuery(queries[idx], []);
			};

			await Promise.all([f(0), f(1)]);

			(res['0'] === undefined).should.be.true();
			(res['1'] === undefined).should.be.true();
			batch.Items.length.should.equal(0);
			db.restore();
		});
	});
	describe('#addToBatch', function () {
		it('should add query to list', async function () {
			let sql = 'SELECT';
			let batch = lib.createBatch(connection, {});
			let operation = new lib.Operation(sql, []);
			batch.addToBatch(operation);

			batch.Quantity.should.equal(1);
			batch.Items.length.should.equal(1);
			batch.Items[0].sql.should.equal(sql);
			batch.stopWaiting();
		});
		it('should throw exception for wrong operation', function () {
			let error = new Error('Fake');
			try {
				let batch = lib.createBatch(connection, {});
				batch.addToBatch({});
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Batch items: Unexpected operation to add');
		});
	});
	describe('#collect', function () {
		it('should add query to list', async function () {
			let sql = 'SELECT';
			let batch = lib.createBatch(connection, {});
			let operation = new lib.Operation(sql, []);
			batch.collect(operation);

			batch.Quantity.should.equal(1);
			batch.Items.length.should.equal(1);
			batch.Items[0].sql.should.equal(sql);
			batch.stopWaiting();
		});
		it('should make a bulk query', async function () {
			let sql = 'INSERT INTO table_name (c1,c2,c3) VALUES (?, ?, ?);';
			let bulkSql = 'INSERT INTO table_name (c1,c2,c3) VALUES (?, ?, ?), (?, ?, ?);';
			let params = [1, 2, 3];

			let batch = lib.createBatch(connection, {});
			batch.collect(new lib.Operation(sql, params));
			batch.collect(new lib.Operation(sql, params));

			batch.Quantity.should.equal(1);
			batch.Items.length.should.equal(1);
			batch.Items[0].sql.should.equal(bulkSql);
			batch.stopWaiting();
		});
	});
});