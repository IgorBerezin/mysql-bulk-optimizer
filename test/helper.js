const mysql = require('mysql');
const sinon = require('sinon');

const test = require('./test');

module.exports = {
	mysqlConfig: {
		host: 'localhost'
	},

	mysqlConnection: function () {
		return mysql.createConnection(module.exports.mysqlConfig);
	},

	sinon: sinon,

	mockMysql: sinon.mock(mysql),

	getFakeMysqlConnection: () => {
		module.exports.mockMysql.expects('createConnection').returns({
			connect: () => {
				console.log('Succesfully connected');
			},
			query: (query, vars, callback) => {
				//console.log('Succesfully query', query, vars);
				callback(null, 'succes');
			},
			end: (callback) => {
				//console.log('Fake Connection ended!', '\r\n');
				callback = callback || function () { }; // do nothing if not defined.
				this.state === 'disconnected';
				callback(null);
			}
		});
		return module.exports.mysqlConnection();
	},

	mockMysqlConnection: (connection) => {
		return sinon.mock(connection);
	},

	spyMysqlConnection: (connection) => {
		return sinon.spy(connection);
	},

	test: test
};