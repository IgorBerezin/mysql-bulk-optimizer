# mysql-bulk-optimizer

NodeJs library for Mysql Query Batch Optimization

## Supported Versions
This SDK stable for node versions 8 and above

## Release Notes
| Version | Notes |
|:--|:--|
| 1.0.0 | Initial |

## Install

Run

```
npm install mysql-bulk-optimizer
```

## Usage

### General principles
This library create separate query optimizer to batch process data into MySQL using bulk operations, if posiible.

It is easy to use optimizer, just need to create a wrap for mysql 'connection' or 'pull' object with options:

* `size` Batch size. Number of queries to collect before process data.
* `time` Maximum time wait before executing or 0

Example:
```Javascript
const mysql = require('mysql');
const optimizer = require('mysql-bulk-optimizer');

const connection = mysql.createConnection({'use node-mysql configuration'});
const batch = optimizer.createBatch(connection, {'size': 2});

let sql = 'INSERT INTO table_name (c1,c2,c3) VALUES (?, ?, ?);';
let bulkSql = 'INSERT INTO table_name (c1,c2,c3) VALUES (?, ?, ?), (?, ?, ?);';
let params = [1, 2, 3];
let cb = () => { };

batch.query(sql, params, cb));
batch.query(sql, params, cb));

```
Mysql query will be executed:
```sql
INSERT INTO table_name (c1,c2,c3) VALUES (?, ?, ?), (?, ?, ?);

with params:
[1, 2, 3, 1, 2, 3]
```
Eaach callback will be applyed with relevant part of result, like it was a separate query call

## Troubleshooting
- We're using Mocha as a test framework
- We can write our JavaScript in ES6, and compile it back to ES5 using Babel
- We can debug our tests using Node's built-in debugger, or using the Chrome DevTools
- We're measuring our test coverage using Istanbul

# Notes

Created as a module for another project. Some features still not implemented.

# License
MIT see LICENSE.

# Author
Igor Berezin <ig.v.berezin@gmail.com>